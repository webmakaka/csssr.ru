import React from 'react';
import Workflow from './index.jsx';
import storiesOf from 'utils/storiesOf';

storiesOf('Workflow')
	.add('default', () => (
		<Workflow />
	));
