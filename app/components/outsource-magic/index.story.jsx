import React from 'react';
import OutsourceMagic from './index.jsx';
import storiesOf from 'utils/storiesOf';

storiesOf('OutsourceMagic')
	.add('default', () => (
		<OutsourceMagic />
	));
