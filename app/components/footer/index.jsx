import React from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Link } from 'react-router';
import Icon from 'components/icon';

import styles from './styles.css';

function Footer() {
	const paymentSystems = [
		'visa',
		'mastercard',
		'yandex',
		'paypal',
		'cashless',
	];
	const linkWithMargin = cx({
		[styles.link]: true,
		[styles.link_indent]: true,
	});

	return (
		<footer className={styles.root}>
			<div className={styles.inner}>
				<div className={styles.payment}>
					{paymentSystems.map((system) => (
						<span className={styles.paymentIcon} key={system}>
							<Icon icon={`payment/${system}`} />
						</span>
					))}
				</div>
				<div className={styles.invite}>
					<span className={styles.invalid}>
						<Icon
							icon='invalid'
						/>
					</span>
					<Link
						className={styles.link}
						to='/jobs'
					>
						Приглашаем на работу
					</Link>
					{' '}
					людей с ограниченными возможностями
				</div>
				<div className={styles.links}>
					<a
						className={linkWithMargin}
						href='http://csssrvice.reformal.ru/'
					>
						Отзывы и предложения
					</a>
					{' '}
					<a
						className={styles.link}
						href='http://blog.csssr.ru'
					>
						Блог трудового коллектива
					</a>
				</div>
				<div className={styles.copyright}>
					© 2011—{(new Date()).getFullYear()} «
					<Link
						className={styles.link}
						to='/'
					>
						CSSSR
					</Link>
					»
				</div>
			</div>
		</footer>
	);
}

export default withStyles(Footer, styles);
