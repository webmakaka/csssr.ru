export const BASE_URL = 'http://test-site.csssr.ru';
export const SITE_URL = 'http://test-site.csssr.ru/uploads/';
export const HR_DOMAIN = 'http://test-hr.csssr.ru';
export const CRM_URL = 'http://test-tools-back.csssr.ru/api/site/order';
export const ORDER_MAIL = 'dzhiriki@gmail.com, nina.smolentseva@csssr.io';
export const NODE_ENV = 'production';
