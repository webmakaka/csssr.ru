export const BASE_URL = 'http://csssr.ru';
export const SITE_URL = 'http://csssr.ru/uploads/';
export const HR_DOMAIN = 'http://hr.csssr.ru';
export const CRM_URL = 'http://space-tools-back.csssr.ru/api/site/order';
export const ORDER_MAIL = 'sales@csssr.io';
export const NODE_ENV = 'production';
