# CSSSR.RU

[![Build Status](https://travis-ci.org/CSSSR/csssr.ru.svg?branch=master)](https://travis-ci.org/CSSSR/csssr.ru)

## Запуск

### `develop` версия

#### Для всех
```
npm start
```

### `staging` версия
```
NODE_ENV=staging npm run production
```

### `production` версия
```
NODE_ENV=staging npm run production
```
